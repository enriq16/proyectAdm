from django.urls import path
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^proyectManager/login$', views.login, name='login'),
    url(r'^proyectManager/menu$', views.menu, name='menu'),
    url('', views.index, name='index'),
]