from django.http import HttpResponse
from django.shortcuts import render
import json
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate
# Create your views here.

def index(request):
    return render(request, 'managerProyect/login.html')

def menu(request):
    return render(request, 'managerProyect/workSpace.html')

@csrf_exempt
def login(request):
    user = request.POST['username']
    passw = request.POST['password']

    response_data = {}
    response_data['resp'] = 'noExito'

    user = authenticate(username=user, password=passw)
    if user is not None:
        response_data['resp'] = 'exito'
    else:
        response_data['err'] = 'Credenciales Incorrectas'

    return HttpResponse(json.dumps(response_data), content_type="application/json")

